<?php

use App\Models\Post;
use App\Models\Role;
use App\Models\Tag;
use App\Models\User;
use App\Models\Video;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('users', function () {
    return User::all();
});

Route::get('users/{user}', function (User $user) {
    return $user->load('address','posts','videos','comments','images');
});

/*
|--------------------------------------------------------------------------
| Many-To-Many
|--------------------------------------------------------------------------
*/

Route::get('users/{user}/attach-role/{role}', function (User $user, Role $role) {
    $user->roles()->attach($role);
    return $user->load('roles');
});

Route::get('users/{user}/detach-role/{role}', function (User $user, Role $role) {
    $user->roles()->detach($role);
    return $user->load('roles');
});

/*
|--------------------------------------------------------------------------
| Has-Many
|--------------------------------------------------------------------------
*/

Route::get('users/{user}/posts', function (User $user) {
    return $user->posts->with('comments','tags');
});

Route::get('posts/{post}', function (Post $post) {
    return $post->load('comments', 'tags');
});

Route::get('users/{user}/videos', function (User $user) {
    return $user->videos->with('comments','tags');
});

Route::get('videos/{videos}', function (Video $video) {
    return $video->load('comments', 'tags');
});

/*
|--------------------------------------------------------------------------
| Polymorphic-One-To-Many
|--------------------------------------------------------------------------
*/

Route::get('posts/{post}/add-comment/{user}', function (Post $post, User $user) {
    return $post->comments()->create([
        'body'    => 'Comment by ' . $user->name,
        'user_id' => $user->id,
    ]);
});

Route::get('videos/{videos}/add-comment/{user}', function (Video $video, User $user) {
    return $video->comments()->create([
        'body'    => 'Comment by ' . $user->name,
        'user_id' => $user->id,
    ]);
});


/*
|--------------------------------------------------------------------------
| Polymorphic-Many-To-Many
|--------------------------------------------------------------------------
*/

Route::get('videos/{videos}/add-tag/{tag}', function (Video $video, Tag $tag) {
    $video->tags()->attach($tag);
    return $video->load('comments', 'tags');
});

/*
|--------------------------------------------------------------------------
| Has-Many-Through
|--------------------------------------------------------------------------
*/

Route::get('users/{user}/images', function (User $user) {
    return $user->images;
});

/*
|--------------------------------------------------------------------------
| Has-One
|--------------------------------------------------------------------------
*/

Route::get('users/{user}/address', function (User $user) {
    return $user->address;
});
