<?php

namespace Database\Seeders;

use App\Models\Address;
use App\Models\Image;
use App\Models\Post;
use App\Models\Project;
use App\Models\Role;
use App\Models\Tag;
use App\Models\Task;
use App\Models\User;
use App\Models\Video;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $users = array([
            'name'     => 'User 1',
            'email'    => 'user1@email.com',
            'password' => Hash::make('password')
        ], [
            'name'     => 'User 2',
            'email'    => 'user2@email.com',
            'password' => Hash::make('password')
        ]);

        User::insert($users);

        $addresses = array([
            'location' => 'Address User 1',
            'user_id'  => 1
        ], [
            'location' => 'Address User 2',
            'user_id'  => 2
        ]);

        Address::insert($addresses);

        $posts = array([
            'title'     => 'Post 1 by User 1',
            'user_id'  => 1

        ], [
            'title'     => 'Post 2 by User 1',
            'user_id'  => 1
        ], [
            'title'     => 'Post 1 by User 2',
            'user_id'  => 2

        ], [
            'title'     => 'Post 2 by User 2',
            'user_id'  => 2
        ]);

        Post::insert($posts);

        $images = array([
            'post_id'  => 1,
        ], [
            'post_id'  => 2,
        ], [
            'post_id'  => 2,
        ], [
            'post_id'  => 3,
        ], [
            'post_id'  => 4,
        ], [
            'post_id'  => 4,
        ]);

        Image::insert($images);


        $videos = array([
            'title'     => 'Video 1 by User 1',
            'user_id'  => 1

        ], [
            'title'     => 'Video 2 by User 1',
            'user_id'  => 1
        ], [
            'title'     => 'Video 1 by User 2',
            'user_id'  => 2

        ], [
            'title'     => 'Video 2 by User 2',
            'user_id'  => 2
        ]);

        Video::insert($videos);

        $roles = array([
            'name'     => 'Role 1',
        ], [
            'name'     => 'Role 2',
        ], [
            'name'     => 'Role 3',
        ]);

        Role::insert($roles);

        DB::table('role_user')->insert([
            'role_id' => 1,
            'user_id' => 1,
        ],[
            'role_id' => 2,
            'user_id' => 1,
        ],[
            'role_id' => 2,
            'user_id' => 2,
        ]);

        $tags = array([
            'name'     => 'Tag 1',
        ], [
            'name'     => 'Tag 2',
        ]);

        Tag::insert($tags);
    }
}
